Connection Exception Example
============================
The purpose of this example is to demonstrate the ability to edit out itineraries
 based on airline connections; similar to the 'Connection Exceptions' found in the Data Explorer.
 
## The Problem

To prevent certain airline connections using 'Connection Exceptions' table could be very verbose.
 
For example, to prevent any 
   
  - AA and A3 connection
   
 'Connection Exceptions' table will be something like:

ORIGIN LEVEL|ORIGIN|DESTINATION LEVEL|DESTINATION|CNCTNUMBER|CARRIER1|CNCT1 LEVEL|CNCT1|CARRIER2|CNCT2 LEVEL|CNCT2|CARRIER3|FREQUENCY 
------------|------|-----------------|-----------|----------|--------|-----------|-----|--------|-----------|-----|--------|---------
 All        |\*    | All             |\*         |1         | AA     | All       |\*   | A3     | All       |\*   |\*      |1234567
 All        |\*    | All             |\*         |1         | A3     | All       |\*   | AA     | All       |\*   |\*      |1234567
 All        |\*    | All             |\*         |2         | AA     | All       |\*   | \*     | All       |\*   | A3     |1234567
 All        |\*    | All             |\*         |2         | A3     | All       |\*   | \*     | All       |\*   | AA     |1234567
 All        |\*    | All             |\*         |2         | \*     | All       |\*   | AA     | All       |\*   | A3     |1234567
 All        |\*    | All             |\*         |2         | \*     | All       |\*   | A3     | All       |\*   | AA     |1234567
 All        |\*    | All             |\*         |2         | AA     | All       |\*   | A3     | All       |\*   |\*      |1234567
 All        |\*    | All             |\*         |2         | A3     | All       |\*   | AA     | All       |\*   |\*      |1234567


For this sample extension point implementation, we just need to specify that (AA, A3) is an invalid combination for connection. 