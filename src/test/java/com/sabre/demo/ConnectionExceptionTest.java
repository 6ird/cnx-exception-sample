package com.sabre.demo;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.sabre.fltsked.extensions.modules.profit.ds.Frequency;
import com.sabre.fltsked.extensions.modules.profit.ds.connectionbuilder.Itinerary;
import com.sabre.fltsked.extensions.modules.profit.ds.connectionbuilder.FlightLeg;
import org.junit.Before;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;

import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.easymock.EasyMock.expect;
import static org.unitils.easymock.EasyMockUnitils.createRegularMock;
import static org.unitils.easymock.EasyMockUnitils.replay;

public class ConnectionExceptionTest extends UnitilsJUnit4
{
    private ConnectionException testedObj;

    @Before
    public void setUp() throws Exception
    {
        testedObj = new ConnectionException()
        {
            @Override
            public void onStart(long analysisId, File analysisDirectory)
            {
                blockList = new HashSet<Set<String>>();
                blockList.add(ImmutableSet.of("AA", "A3"));
                blockList.add(ImmutableSet.of("AA", "AF"));
            }
        };
        testedObj.onStart(1, null);
    }

    @Test
    public void test() throws Exception
    {
        final Itinerary valid1 = itin("AF", "A3");
        final Itinerary valid2 = itin("AF", "A3", "UA");
        final Itinerary invalid1 = itin("AF", "A3", "AA");
        final Itinerary invalid2 = itin("AF", "A3", "UA", "AA");
        replay();
        final Map<Itinerary, Frequency> result = testedObj.getValidFrequencies("ORG", "DST", ImmutableList.of(invalid1, valid1, valid2, invalid2));
        assertEquals(2, result.size());
        assertTrue(result.get(invalid1).asSet().isEmpty());
        assertTrue(result.get(invalid2).asSet().isEmpty());
    }

    private static Itinerary itin(String... airlines)
    {
        final ImmutableList.Builder<FlightLeg> legBuilder = ImmutableList.builder();
        for (String airline : airlines)
        {
            legBuilder.add(leg(airline));
        }
        final Itinerary itin = createRegularMock(Itinerary.class);
        expect(itin.getLegs()).andStubReturn(legBuilder.build());
        return itin;
    }

    private static FlightLeg leg(String airline)
    {
        final FlightLeg leg = createRegularMock(FlightLeg.class);
        expect(leg.getAirline()).andStubReturn(airline);
        return leg;
    }
}